/*daily reports*/
ALTER PROCEDURE REPORT1
@DATE DATE 
AS 
BEGIN
SELECT COUNT(T.OrderID) AS [Total Orders ] , SUM(t.TotalAmount) as [Total Sale] FROM Bill AS T WHERE @DATE = T.[DATE]
END

/*Monthly reports*/
CREATE PROCEDURE REPORT2
@DATE int 
AS 
BEGIN
SELECT COUNT(T.OrderID) AS [Total Orders ] , SUM(t.TotalAmount) as [Total Sale] FROM Bill AS T WHERE @DATE = MONTH(t.date)
END

/*BEST SELLING PRODUCT*/
CREATE PROCEDURE REPORT3
AS 
BEGIN
SELECT ProductID , SUM(QUANTITY) AS Quantity FROM [Order Details] AS T GROUP BY ProductID HAVING SUM(Quantity) >= (SELECT SUM(Quantity) FROM [Order Details] AS T GROUP BY ProductID )
END

/*sale of products*/
CREATE PROCEDURE REPORT4
AS 
BEGIN
SELECT ProductID , COUNT(Quantity) AS Quantity FROM [Order Details] AS T GROUP BY ProductID
END

/*ATTENDANCE STATUS MONTHLY*/
ALTER PROCEDURE REPORT5
@Month INT
AS 
BEGIN
SELECT EmployeeID , (SELECT Status WHERE T1.Status = 1) AS PRESENTS , (SELECT Status WHERE T1.Status = 0) AS ABSENTS FROM  (SELECT EmployeeID , Status  FROM [Attendence Details]  WHERE AttendenceID IN (SELECT AttendenceID FROM Attendence  AS T  WHERE month(t.DateCreated) = @Month) GROUP BY EmployeeID , Status) AS T1
END 

/*SANITIZATION STATUS*/
ALTER PROCEDURE REPORT6 
@Month INT
AS
BEGIN
SELECT T.SanitizationID , T.EmployeeID , T.DateAchieved , T.Achieved FROM (SELECT T1.SanitizationID , T1.DateAchieved , T1.Achieved , T1.EmployeeID  FROM [Sanitization Details] AS T1 WHERE T1.SanitizationID IN (SELECT SanitizationID FROM [Sanitization] as t2 WHERE month(t2.SanitizationDate) = @Month)) AS T
END
