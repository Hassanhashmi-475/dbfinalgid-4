---------------------------------------------------------Bill------------------------------------------------------------
ALTER TRIGGER GeneratingBill ON ORDERS
AFTER INSERT 
AS 
BEGIN
DECLARE @OID INT
DECLARE @CID INT
DECLARE @DT DATETIME
DECLARE @AMOUNT INT 
DECLARE @DP INT
DECLARE @DID INT
SELECT @OID = I.ORDERID  , @CID = I.CustomerID , @DT = I.OrderDate  FROM inserted I;
SET @AMOUNT = DBO.BillSum(@OID)
SET @DP = DBO.GETDISCOUNT(@DT , @AMOUNT)
SET @DID = DBO.GetDisscountID(@DT , @AMOUNT)
INSERT INTO BILL(CUSTOMERID,ORDERID,DISCOUNTID,DISCOUNT,TOTALAMOUNT,DATE) VALUES (@CID,@OID,@DID,@DP,@AMOUNT,@DT)
END;

-----------------------------------------------------Sanitization---------------------------------------------------------------------------
CREATE TRIGGER Sanitized ON [Sanitization Details]
INSTEAD OF INSERT
AS
BEGIN
DECLARE @SID INT
DECLARE @DueDate DATETIME
DECLARE @Date DATETIME
DECLARE @EID INT
DECLARE @Done NVARCHAR(20)
SELECT @SID = I.SanitizationID ,@EID = I.EmployeeID  FROM inserted I
SELECT @DueDate = SanitizationDate FROM Sanitization WHERE @SID = SanitizationID
SET @Date = GETDATE()
SET @Done = DBO.DeadLine(@DueDate , @Date)
INSERT INTO SanitizationDetaiLs(EMPLOYEEID,SANITIZATIONID,[DateAchieved],ACHIEVED) VALUES(@EID,@SID,@Date,@Done)
END

--------------------------------------------------------------PRODUCTS------------------------------------------------------------------
ALTER TRIGGER UpdateQuantity ON [Order Details] 
AFTER INSERT
AS
BEGIN
DECLARE @PID INT
DECLARE @Quantity INT
SELECT @PID = I.ProductID , @Quantity = I.Quantity FROM INSERTED I
UPDATE [Products] SET UnitsInStock = UnitsInStock - @Quantity WHERE @PID = ProductID
END

----------------------------------------------------------RecipeDetails---------------------------------------------------------------
CREATE TRIGGER ManageRecipes ON Products
INSTEAD OF DELETE
AS
BEGIN
DECLARE @PID INT 
SELECT @PID = I.ProductID FROM DELETED I;
DELETE [Recipie Details] WHERE @PID = ProductID
DELETE[Products] WHERE @PID = ProductID
END
----------------------------------------------------------------------------------------------------------------------------------------