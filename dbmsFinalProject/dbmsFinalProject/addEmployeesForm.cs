﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace dbmsFinalProject
{
    public partial class addEmployeesForm : Form
    {

                        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                        private static extern IntPtr CreateRoundRectRgn
                (
                int nLeftRect,
                int nTopRect,
                int nRightRect,
                int nBottomRect,
                int nWidthEllipse,
                int nHeightEllipse

                );



        public addEmployeesForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            managerForm page = new managerForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void addEmployeesForm_Load(object sender, EventArgs e)
        {
            //Employee Get names
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select UserName as UName,UserID as ID from users where UserID NOt IN (SELECT ManagerID FROM Manager)", con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            usernameComboBox.DisplayMember = "UName";
            usernameComboBox.ValueMember = "ID";
            usernameComboBox.DataSource = dt;



            // Shift of Employees
            SqlCommand cmd1 = new SqlCommand("Select Value as Val,ID as Idd from Lookup where Category='Shift' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da.Fill(dt1);
            shiftComboBox.DisplayMember = "Val";
            shiftComboBox.ValueMember = "Idd";
            shiftComboBox.DataSource = dt1;


            //Designation
            SqlCommand cmd2 = new SqlCommand("Select Value as Vall,ID as Id from Lookup where Category='Designation' ", con);
            SqlDataAdapter daa = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            daa.Fill(dt2);
            designationComboBox.DisplayMember = "Vall";
            designationComboBox.ValueMember = "Id";
            designationComboBox.DataSource = dt2;






        }

        private void addButton_Click(object sender, EventArgs e)
        {


            if (shiftComboBox.Text == "") { MessageBox.Show("Please enter the shift "); }
            else if (designationComboBox.Text == "") { MessageBox.Show("Please enter the designation "); }
            else if (salaryTextBox.Text == "") { MessageBox.Show("Please enter the Salary "); }
            else if (System.Text.RegularExpressions.Regex.IsMatch(phoneTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                phoneTextBox.Text = phoneTextBox.Text.Remove(phoneTextBox.Text.Length - 1);

            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(salaryTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                salaryTextBox.Text = salaryTextBox.Text.Remove(salaryTextBox.Text.Length - 1);

            }
            else if (phoneTextBox.Text.Length < 11 || phoneTextBox.Text.Length > 11) { MessageBox.Show("Phone must be of 11 Numbers"); }
            else
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Employees values (@EmployeeID,@BirthDate,@HireDate,@Address,@Shift,@Designation,@Salary,@Phone)", con);

                cmd.Parameters.AddWithValue("@EmployeeID", usernameComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@BirthDate", DateTime.Parse(birthDateDateTimePicker.Text));
                cmd.Parameters.AddWithValue("@HireDate", DateTime.Parse(hirDateDateTimePicker.Text));
                cmd.Parameters.AddWithValue("@Address", addressTextBox.Text);
                cmd.Parameters.AddWithValue("@Shift", shiftComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@Designation", designationComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@Salary", salaryTextBox.Text);
                cmd.Parameters.AddWithValue("@Phone", phoneTextBox.Text);

                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Saved");
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from [Employees] where EmployeeID=@EmployeeID ", con);
            cmd1.Parameters.AddWithValue("@EmployeeID", usernameComboBox.SelectedValue);
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Data");
            usernameComboBox.Text="";
        }

        private void usernameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select BirthDate,HireDate,Address,Shift,Designation,Salary,Phone from [Employees] where EmployeeID=@EmployeeID", con);
            cmd.Parameters.AddWithValue("@EmployeeID", usernameComboBox.SelectedValue);

            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {

                birthDateDateTimePicker.Text = da.GetValue(0).ToString();
                hirDateDateTimePicker.Text = da.GetValue(1).ToString();
                addressTextBox.Text = da.GetValue(2).ToString();
                shiftComboBox.SelectedValue = da.GetValue(3).ToString();
                designationComboBox.SelectedValue = da.GetValue(4).ToString();
                salaryTextBox.Text = da.GetValue(5).ToString();
                phoneTextBox.Text = da.GetValue(6).ToString();

            }
            if (usernameComboBox.Text == "")
            {
                birthDateDateTimePicker.ResetText();
                hirDateDateTimePicker.ResetText();
                addressTextBox.Clear();
                shiftComboBox.Text = "";
                designationComboBox.Text = "";
                salaryTextBox.Clear();
                phoneTextBox.Clear();

            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {

            if (shiftComboBox.Text == "") { MessageBox.Show("Please enter the shift "); }
            else if (designationComboBox.Text == "") { MessageBox.Show("Please enter the designation "); }
            else if (salaryTextBox.Text == "") { MessageBox.Show("Please enter the Salary "); }
            else if (System.Text.RegularExpressions.Regex.IsMatch(phoneTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter valid amount in salary");
                phoneTextBox.Text = phoneTextBox.Text.Remove(phoneTextBox.Text.Length - 1);

            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(salaryTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in phone");
                salaryTextBox.Text = salaryTextBox.Text.Remove(salaryTextBox.Text.Length - 1);

            }
            else if (phoneTextBox.Text.Length < 11 || phoneTextBox.Text.Length > 11) { MessageBox.Show("Phone must be of 11 Numbers"); }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update  Employees set BirthDate=@BirthDate,HireDate=@HireDate,Address=@Address,Shift=@Shift,Designation=@Designation,Salary=@Salary,Phone=@Phone where EmployeeID=@EmployeeID", con);

                cmd.Parameters.AddWithValue("@EmployeeID", usernameComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@BirthDate", DateTime.Parse(birthDateDateTimePicker.Text));
                cmd.Parameters.AddWithValue("@HireDate", DateTime.Parse(hirDateDateTimePicker.Text));
                cmd.Parameters.AddWithValue("@Address", addressTextBox.Text);
                cmd.Parameters.AddWithValue("@Shift", shiftComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@Designation", designationComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@Salary", salaryTextBox.Text);
                cmd.Parameters.AddWithValue("@Phone", phoneTextBox.Text);

                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully updated");
            }
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Employees] ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            employeesDataGridView.DataSource = dt;
        }
    }
}
