﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Collections;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Runtime.InteropServices;



namespace dbmsFinalProject
{
    public partial class takeOrdersForm : Form
    {



                        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                        private static extern IntPtr CreateRoundRectRgn
                (
                int nLeftRect,
                int nTopRect,
                int nRightRect,
                int nBottomRect,
                int nWidthEllipse,
                int nHeightEllipse

                );
        public takeOrdersForm()
        {
            
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

            

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            employeeForm page = new employeeForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

      /*  private void showOrder() 
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Select * from Orders", con);
            SqlDataAdapter daaa = new SqlDataAdapter(cmd3);
            DataTable dt22 = new DataTable();
            daaa.Fill(dt22);
            ordersDataGridView.DataSource = dt22;

        } */

       private void showOrderDetails( ) 
        {
            var con = Configuration.getInstance().getConnection();


          



            SqlCommand cmd4 = new SqlCommand("Select * from [Order Details]", con);
            SqlDataAdapter d4 = new SqlDataAdapter(cmd4);
            DataTable dt4 = new DataTable();
            d4.Fill(dt4);
            orderDetailsDataGridView.DataSource = dt4;

        } 

        private void showBilldetails() 
        {
            var con = Configuration.getInstance().getConnection();
             SqlCommand cmd4 = new SqlCommand("Select distinct OD.OrderID,ProductName,OD.UnitPrice,Quantity,sum(OD.UnitPrice * Quantity ) as [Total Amount]  from Products P join [Order Details] OD on P.ProductID=OD.ProductID where OD.OrderID = " + billorderBox.Text + "group by ProductName,OD.UnitPrice,Quantity,OD.OrderID", con);
            //SqlCommand cmd4 = new SqlCommand("Select sum([Total Amount])as [Sub Total] from Billview", con);
             SqlDataAdapter d4 = new SqlDataAdapter(cmd4);
            DataTable dt4 = new DataTable();
            d4.Fill(dt4);
            ProductBillDatagridview.DataSource = dt4;
        }

        private void showCustomers() 
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd2 = new SqlCommand("Select * from Customers", con);
            SqlDataAdapter daa = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            daa.Fill(dt2);
           // customersDataGridView.DataSource = dt2;
        }
             

        private void takeOrdersForm_Load(object sender, EventArgs e)
        {
            //Employee Get names who are added by manager
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select UserName as UName,EmployeeID as ID from Employees join users on EmployeeID=UserID where UserID NOt IN (SELECT ManagerID FROM Manager)", con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            employeeIDComboBox.DisplayMember = "UName";
            employeeIDComboBox.ValueMember = "ID";
            employeeIDComboBox.DataSource = dt;

            
            //Product Name get
            
            SqlCommand cmd1 = new SqlCommand("Select [ProductName] as PName,[ProductID] as ID from Products ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da.Fill(dt1);
            productIDComboBox.DisplayMember = "PName";
            productIDComboBox.ValueMember = "ID";
            productIDComboBox.DataSource = dt1;


            // Customers Show in Datagrid View
            showCustomers();

            // Orders Show in Datagrid View
           // showOrder();

            //Order Details Show in DataGrid View
            showOrderDetails();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();



            if (employeeIDComboBox.Text == "" || productIDComboBox.Text == "" || addressTextBox.Text == "" || phoneTextBox.Text == "" || quantityTextBox.Text == "" || priceBox.Text == "" || orderDateDateTimePicker.Text == "")
            {
                MessageBox.Show("Please fill the all  fields");
            }

            else if (System.Text.RegularExpressions.Regex.IsMatch(phoneTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in Stock ");
                phoneTextBox.Text = phoneTextBox.Text.Remove(phoneTextBox.Text.Length - 1);

            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(quantityTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter valid  number in Quantity field");
                quantityTextBox.Text = quantityTextBox.Text.Remove(quantityTextBox.Text.Length - 1);

            }

            else
            {
                //Checking Stock
                int a = int.Parse(quantityTextBox.Text);
            SqlCommand cmd11 = new SqlCommand("Select [UnitsInStock] from Products where ProductID=@Id", con);
            cmd11.Parameters.AddWithValue("@Id", productIDComboBox.SelectedValue);
            SqlDataAdapter da = new SqlDataAdapter(cmd11);
            DataTable dt = new DataTable();
            da.Fill(dt);
            orderDetailsDataGridView.DataSource = dt;
            int stock = int.Parse(orderDetailsDataGridView.Rows[0].Cells[0].Value.ToString());








                /////////////////////////////////////////////////////////////////////////////////
                ///
               

              //  SqlCommand commad = new SqlCommand("SET TRANSACTION ISOLATION LEVEL REPEATABLE READ", con);
                //SqlTransaction transaction = con.BeginTransaction();
                try
                {
                    if (a <= stock)
                    {
                        //Customers Add

                        SqlCommand cmd = new SqlCommand("Insert into Customers values(@Address,@Phone)", con);
                        cmd.Parameters.AddWithValue("@Address", addressTextBox.Text);
                        cmd.Parameters.AddWithValue("@Phone", phoneTextBox.Text);
                        cmd.ExecuteNonQuery();


                        // Visiblity of Order and Customer Box
                        OrderTextBox.Visible = true;
                        CustomertextBox.Visible = true;
                        OrderLabel.Visible = true;
                        CustomerLabel.Visible = true;
                        AddOrderBTn.Visible = true;



                        //Insert Customer  into TextBox automatically 
                        SqlCommand Comm1 = new SqlCommand("Select top(1) CustomerID from Customers order by  CustomerID Desc", con);
                        SqlDataReader DR1 = Comm1.ExecuteReader();
                        if (DR1.Read())
                        {
                            CustomertextBox.Text = DR1.GetValue(0).ToString();
                        }
                        MessageBox.Show("Customer ID is = " + CustomertextBox.Text);


                        //Inserting into Order
                        SqlCommand cmd1 = new SqlCommand(" Insert into Orders values(@CustomerID,@EmployeeID,@OrderDate)", con);
                        cmd1.Parameters.AddWithValue("@CustomerID", int.Parse(CustomertextBox.Text));

                        // recent employee ID
                        cmd1.Parameters.AddWithValue("@EmployeeID", employeeIDComboBox.SelectedValue);
                        cmd1.Parameters.AddWithValue("@OrderDate", DateTime.Parse(orderDateDateTimePicker.Text));
                        cmd1.ExecuteNonQuery();


                        //Insert CustomerID  into TextBox automatically 
                        SqlCommand Comm = new SqlCommand("Select top(1) OrderID from Orders order by OrderID Desc", con);
                        SqlDataReader DR2 = Comm.ExecuteReader();
                        if (DR2.Read())
                        {
                            OrderTextBox.Text = DR2.GetValue(0).ToString();
                        }
                        MessageBox.Show("Order ID is = " + OrderTextBox.Text);




                        // Update stock in Prodcucts
                        SqlCommand command = new SqlCommand("update Products set UnitsInStock=@UnitsInStock  where ProductID=@Id", con);
                        command.Parameters.AddWithValue("@Id", productIDComboBox.SelectedValue);
                        command.Parameters.AddWithValue("@UnitsInStock", stock - a);
                        command.ExecuteNonQuery();

                        //Inserting Order Details
                        SqlCommand cmd2 = new SqlCommand("Insert into [Order Details] values(@OrderID,@ProductID,@UnitPrice,@Quantity)", con);
                        cmd2.Parameters.AddWithValue("@OrderID", OrderTextBox.Text);
                        cmd2.Parameters.AddWithValue("@ProductID", productIDComboBox.SelectedValue);
                        cmd2.Parameters.AddWithValue("@UnitPrice", int.Parse(priceBox.Text));
                        cmd2.Parameters.AddWithValue("@Quantity", int.Parse(quantityTextBox.Text));
                        cmd2.ExecuteNonQuery();


                       
;                        /////////////////////////////////////////////////////////////////////////////////////////////
                        MessageBox.Show("Succesfully Takes the Order of Customer");

                        createOrderBtn.Visible = true;

                        // showOrder();
                        showOrderDetails();
                        showCustomers();


                        addressTextBox.Clear();
                        phoneTextBox.Clear();
                        quantityTextBox.Clear();
                        priceBox.Clear();
                       // transaction.Commit();
                        MessageBox.Show("Transaction Succesful");

                    }


                    else { MessageBox.Show("Sorry we have only " + stock + " items of this type"); }

                }
                catch {// transaction.Rollback();
                    MessageBox.Show("Transaction Failed"); }
            }
        }



        private void createOrderBtn_Click(object sender, EventArgs e)
        {
            CustomertextBox.Visible = false;
            CustomerLabel.Visible = false;
            addressTextBox.Visible = false;
            phoneTextBox.Visible = false;
            addButton.Visible = false;
            orderDateDateTimePicker.Visible = false;
            label7.Visible = false;
            label5.Visible = false;
            label9.Visible = false;

        }

        private void AddOrderBTn_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(OrderTextBox.Text);
            var con = Configuration.getInstance().getConnection();



            //Checking Stock
            int a = int.Parse(quantityTextBox.Text);
            SqlCommand cmd11 = new SqlCommand("Select [UnitsInStock] from Products where ProductID=@Id", con);
            cmd11.Parameters.AddWithValue("@Id", productIDComboBox.SelectedValue);
            SqlDataAdapter da = new SqlDataAdapter(cmd11);
            DataTable dt = new DataTable();
            da.Fill(dt);
            orderDetailsDataGridView.DataSource = dt;
            int stock = int.Parse(orderDetailsDataGridView.Rows[0].Cells[0].Value.ToString());

            if (a <= stock)
            {
                // Update stock in Prodcucts
                SqlCommand command = new SqlCommand("update Products set UnitsInStock=@UnitsInStock  where ProductID=@Id", con);
                command.Parameters.AddWithValue("@Id", productIDComboBox.SelectedValue);
                command.Parameters.AddWithValue("@UnitsInStock", stock - a);
                command.ExecuteNonQuery();


                SqlCommand cmd2 = new SqlCommand("Insert into [Order Details] values(@OrderID,@ProductID,@UnitPrice,@Quantity)", con);
                cmd2.Parameters.AddWithValue("@OrderID", OrderTextBox.Text);

                cmd2.Parameters.AddWithValue("@ProductID", productIDComboBox.SelectedValue);
                cmd2.Parameters.AddWithValue("@UnitPrice", int.Parse(priceBox.Text));
                cmd2.Parameters.AddWithValue("@Quantity", int.Parse(quantityTextBox.Text));
                cmd2.ExecuteNonQuery();
                showOrderDetails();

                quantityTextBox.Clear();
                priceBox.Clear();


            }

            
            else
            {
                quantityTextBox.Clear();
               // MessageBox.Show("Re write the quantity");
                MessageBox.Show("Sorry we have only " + stock + " items of this type,Re write the quantity");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            takeOrdersForm t = new takeOrdersForm();
            t.Closed += (s, args) => this.Close();
            t.Show();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("Delete from   [Order Details] ", con);
            SqlCommand cmd2 = new SqlCommand("Delete from Orders  ", con);
            SqlCommand cmd1 = new SqlCommand("Delete from Customers  ", con);


            cmd3.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Data");
           
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            showCustomers();
           // showOrder();
            showOrderDetails();
        }

        private void productIDComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select [UnitPrice] from [Products] where ProductID=@ProductID", con);
            cmd.Parameters.AddWithValue("@ProductID", productIDComboBox.SelectedValue);

            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {

                priceBox.Text = da.GetValue(0).ToString();

            }
            if (productIDComboBox.Text == "")
            {
                priceBox.Clear();
            }
            }


        //Bill 
            private void billbtn_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand Comm = new SqlCommand("Select top(1) OrderID from Orders order by OrderID Desc", con);
            SqlDataReader DR = Comm.ExecuteReader();
            if (DR.Read())
            {
                billorderBox.Text = DR.GetValue(0).ToString();
            }
           // if (OrderTextBox.Text != "")
            //{

                SqlCommand cmd = new SqlCommand("Select sum(UnitPrice * Quantity ) as [Total Amount] from [Order Details] join Orders on  [Order Details].OrderID = Orders.OrderID  where [Order Details].OrderID = " + billorderBox.Text + " group by Orders.OrderID", con);
                SqlDataReader DR2 = cmd.ExecuteReader();
                if (DR2.Read())
                {
                    billtextBox.Text = DR2.GetValue(0).ToString();
                }



            showBilldetails();
            generateBill();
          //  InsertBill();
           // }
           // else { MessageBox.Show("There is no current order"); }



        }

        int[] productID = new int[50];
        int[] Quantity = new int[50];
        int[] Price = new int[50];




        private void reportsCb_SelectedIndexChanged(object sender, EventArgs e)
        { }

            private void generateBill()
        {


            if (ProductBillDatagridview.Rows.Count > 0)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "PDF (*.pdf)|*.pdf";
                save.FileName = "Report.pdf";
                bool msg = false;
                if (save.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(save.FileName))
                    {
                        try
                        {
                            File.Delete(save.FileName);
                        }
                        catch (Exception messg)
                        {

                            msg = true;
                            MessageBox.Show("Unable to wride data in disk" + messg.Message);
                        }
                    }
                    if (!msg)
                    {
                        try
                        {
                            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);

                            PdfPTable t = new PdfPTable(ProductBillDatagridview.Columns.Count);
                            t.DefaultCell.Padding = 2;
                            t.WidthPercentage = 100;
                            t.HorizontalAlignment = Element.ALIGN_LEFT;

                            foreach (DataGridViewColumn col in ProductBillDatagridview.Columns)
                            {
                                PdfPCell pCell = new PdfPCell(new Phrase(col.HeaderText));
                                t.AddCell(pCell);
                            }
                            foreach (DataGridViewRow viewRow in ProductBillDatagridview.Rows)
                            {
                                foreach (DataGridViewCell dcell in viewRow.Cells)
                                {
                                        
                                        t.AddCell(dcell.Value?.ToString());

                                }
                            }


                            using (FileStream fileStream = new FileStream(save.FileName, FileMode.Create))
                            {
                                Document document = new Document(PageSize.A5, 8f, 16f, 16f, 8f);
                                PdfWriter.GetInstance(document, fileStream);
                                document.Open();

                                /*
                                Paragraph header = new Paragraph("DEPARTMENT OF COMPUTER SCIENCE", new iTextSharp.text.Font(bfTimes, 24, 4, new iTextSharp.text.BaseColor(10, 10, 10)));
                                header.Alignment = Element.ALIGN_CENTER;
                                document.Add(header);
                                Paragraph subheader = new Paragraph("University of Engineering and Techonolgy, Lahore", new iTextSharp.text.Font(bfTimes, 18, 1, new iTextSharp.text.BaseColor(10, 10, 10)));
                                subheader.Alignment = Element.ALIGN_CENTER;
                                document.Add(subheader); 
                                */

                                Paragraph subheaderText = new Paragraph("Bakery Management System".ToUpper(), new iTextSharp.text.Font(bfTimes, 22, 1, new iTextSharp.text.BaseColor(40, 40, 40)));
                                subheaderText.Alignment = Element.ALIGN_CENTER;
                                subheaderText.SpacingBefore = 10;
                                document.Add(subheaderText);

                               // Paragraph headingReport = new Paragraph(reportsCb.GetItemText(reportsCb.SelectedItem).ToString(), new iTextSharp.text.Font(bfTimes, 16, 1, new iTextSharp.text.BaseColor(40, 40, 40)));
                               // headingReport.Alignment = Element.ALIGN_CENTER;
                                //document.Add(headingReport);

                                Paragraph subheadertext = new Paragraph("Bill", new iTextSharp.text.Font(bfTimes, 18, 4, new iTextSharp.text.BaseColor(40, 40, 40)));
                                subheadertext.Alignment = Element.ALIGN_CENTER;
                                document.Add(subheadertext);

                               
                                //discountbox.SelectedValue
                                
                                t.SpacingBefore = 30;
                                t.HorizontalAlignment = Element.ALIGN_CENTER;
                                document.AddAuthor("Hassan Hashmi");
                                document.AddCreationDate();
                                document.Add(t);

                                if (discountbox.Text != "")
                                {

                                    int b = int.Parse(billtextBox.Text);
                                    int c = int.Parse(discountbox.SelectedValue?.ToString()) * b/100 ;
                                    int a = int.Parse(billtextBox.Text) - c;

                                    Paragraph subfootertext2 = new Paragraph("SubTotal :" + billtextBox.Text+"                   "+orderDateDateTimePicker.Text, new iTextSharp.text.Font(bfTimes, 18, 1, new iTextSharp.text.BaseColor(20, 20, 20)));
                                    subfootertext2.Alignment = Element.ALIGN_LEFT;
                                    subfootertext2.Alignment = Element.ALIGN_BOTTOM;
                                    subfootertext2.SpacingBefore = 20;
                                    document.Add(subfootertext2);

                                    Paragraph subfootertext1 = new Paragraph("Total discount :" + discountbox.SelectedValue?.ToString() + " %", new iTextSharp.text.Font(bfTimes, 18, 1, new iTextSharp.text.BaseColor(20, 20, 20)));
                                    subfootertext1.Alignment = Element.ALIGN_LEFT;
                                    subfootertext1.Alignment = Element.ALIGN_BOTTOM;
                                    subfootertext1.SpacingBefore = 10;
                                    document.Add(subfootertext1);

                                    document.AddTitle("Bill (Bakery Management System)");
                                    Paragraph subfootertext = new Paragraph("Total Bill :" + a, new iTextSharp.text.Font(bfTimes, 18, 1, new iTextSharp.text.BaseColor(20, 20, 20)));
                                    subfootertext.Alignment = Element.ALIGN_LEFT;
                                    subfootertext.Alignment = Element.ALIGN_BOTTOM;
                                    subfootertext.SpacingBefore = 10;
                                    document.Add(subfootertext);
                                    document.Close();
                                    fileStream.Close();
                                }

                                else 
                                {
                                    //int a = int.Parse(billtextBox.Text) - int.Parse(discountbox.SelectedValue?.ToString());

                                    Paragraph subfootertext2 = new Paragraph("SubTotal :" + billtextBox.Text + "                   " + orderDateDateTimePicker.Text, new iTextSharp.text.Font(bfTimes, 18, 1, new iTextSharp.text.BaseColor(20, 20, 20)));
                                    subfootertext2.Alignment = Element.ALIGN_LEFT;
                                    subfootertext2.Alignment = Element.ALIGN_BOTTOM;
                                    subfootertext2.SpacingBefore = 20;
                                    document.Add(subfootertext2);

                                    Paragraph subfootertext1 = new Paragraph("Total discount : 0 %", new iTextSharp.text.Font(bfTimes, 18, 1, new iTextSharp.text.BaseColor(20, 20, 20)));
                                    subfootertext1.Alignment = Element.ALIGN_LEFT;
                                    subfootertext1.Alignment = Element.ALIGN_BOTTOM;
                                    subfootertext1.SpacingBefore = 10;
                                    document.Add(subfootertext1);

                                    document.AddTitle("Bill (Bakery Management System)");
                                    Paragraph subfootertext = new Paragraph("Total Bill :" + billtextBox.Text, new iTextSharp.text.Font(bfTimes, 18, 1, new iTextSharp.text.BaseColor(20, 20, 20)));
                                    subfootertext.Alignment = Element.ALIGN_LEFT;
                                    subfootertext.Alignment = Element.ALIGN_BOTTOM;
                                    subfootertext.SpacingBefore = 10;
                                    document.Add(subfootertext);
                                    document.Close();
                                    fileStream.Close();
                                }



                            }
                            MessageBox.Show("  Successfully", "info");

                        }

                        catch (Exception messg)
                        {

                            MessageBox.Show("There is an error in exporting data "  + messg.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Nothing Found", "Info");


            }




        }

        private void LoadDiscountBtn_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd1 = new SqlCommand("Select [DiscountName] as PName,[Discount Value] as Dval from Discounts ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da.Fill(dt1);
            discountbox.DisplayMember = "PName";
            discountbox.ValueMember = "Dval";
            discountbox.DataSource = dt1;
        }

        private void InsertBill() 
        {

            
           // DiscountID();
            if (label11.Text == "" && discountbox.SelectedValue == null)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Bill values (@OrderID,@DiscountID,@Discount,@TotalAmount)", con);
                cmd.Parameters.AddWithValue("@OrderID", OrderTextBox.Text);
                cmd.Parameters.AddWithValue("@DiscountID", null);
                cmd.Parameters.AddWithValue("@Discount", null);
                cmd.Parameters.AddWithValue("TotalAmount", billtextBox.Text);
                cmd.ExecuteNonQuery();
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Bill values (@OrderID,@DiscountID,@Discount,@TotalAmount)", con);
                cmd.Parameters.AddWithValue("@OrderID", OrderTextBox.Text);
                cmd.Parameters.AddWithValue("@DiscountID", label11.Text);
                cmd.Parameters.AddWithValue("@Discount", discountbox.SelectedValue);
                cmd.Parameters.AddWithValue("TotalAmount", billtextBox.Text);
                cmd.ExecuteNonQuery();
            }

        }

        private void DiscountID() 
        {
            MessageBox.Show(discountbox.Text);
            var con = Configuration.getInstance().getConnection();

            if (discountbox.Text!="") { 
            SqlCommand Comm = new SqlCommand("Select DiscountID from  Discounts  where DiscountName =" +  discountbox.Text, con);
            SqlDataReader DR2 = Comm.ExecuteReader();
            if (DR2.Read())
            {
                label11.Text = DR2.GetValue(0).ToString();
            }
            }
        }

    }













}
