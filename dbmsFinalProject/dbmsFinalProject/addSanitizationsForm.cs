﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;



namespace dbmsFinalProject
{
    public partial class addSanitizationsForm : Form
    {


                    [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                    private static extern IntPtr CreateRoundRectRgn
            (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse

            );

        public addSanitizationsForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            managerForm page = new managerForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addSanitizationsForm_Load(object sender, EventArgs e)
        {
            
          
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from [Sanitization] where SanitizationLevel=@SanitizationLevel", con);
            cmd1.Parameters.AddWithValue("@SanitizationLevel", sanitizationLevelTextBox.Text);
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Data");
            sanitizationLevelTextBox.Clear();
        }

        private void sanitizationLevelTextBox_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select  Description,SanitizationDate from [Sanitization] where  SanitizationLevel=@SanitizationLevel", con);
            cmd.Parameters.AddWithValue("@SanitizationLevel", sanitizationLevelTextBox.Text);

            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {

                DescriptionTextBox.Text = da.GetValue(0).ToString();
                sanitizationDateDateTimePicker.Text = da.GetValue(1).ToString();



            }
            if (sanitizationLevelTextBox.Text == "")
            {

                DescriptionTextBox.Clear();
                sanitizationDateDateTimePicker.ResetText();

            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {


            if (sanitizationLevelTextBox.Text == "" || DescriptionTextBox.Text == "" || sanitizationDateDateTimePicker.Text == "")
            {
                MessageBox.Show("Please fill all the fields");
            }

            else if (System.Text.RegularExpressions.Regex.IsMatch(sanitizationLevelTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in sanitization level");
                sanitizationLevelTextBox.Text = sanitizationLevelTextBox.Text.Remove(sanitizationLevelTextBox.Text.Length - 1);

            }
            else
            {


                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update  [Sanitization] set  Description=@des,SanitizationDate=@Date where SanitizationLevel=@SanitizationLevel", con);
                cmd.Parameters.AddWithValue("@SanitizationLevel", int.Parse(sanitizationLevelTextBox.Text));
                cmd.Parameters.AddWithValue("@des", DescriptionTextBox.Text);
                cmd.Parameters.AddWithValue("@Date", DateTime.Parse(sanitizationDateDateTimePicker.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully updated");
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            if (sanitizationLevelTextBox.Text == "" || DescriptionTextBox.Text == "" || sanitizationDateDateTimePicker.Text == "")
            {
                MessageBox.Show("Please fill all the fields");
            }

            else if (System.Text.RegularExpressions.Regex.IsMatch(sanitizationLevelTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in sanitization level");
                sanitizationLevelTextBox.Text = sanitizationLevelTextBox.Text.Remove(sanitizationLevelTextBox.Text.Length - 1);

            }
            else
            {
                try
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand("Insert into  [Sanitization] values (@SanitizationLevel,@Description,@SanitizationDate)", con);
                    cmd.Parameters.AddWithValue("@SanitizationLevel", Int32.Parse(sanitizationLevelTextBox.Text));
                    cmd.Parameters.AddWithValue("@Description", DescriptionTextBox.Text);
                    cmd.Parameters.AddWithValue("@SanitizationDate", DateTime.Parse(sanitizationDateDateTimePicker.Text));
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully saved");
                }
                catch(SqlException ex) { MessageBox.Show(ex.Message); }
            }
        }

        private void showButton_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Sanitization]  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            sanitizationsDataGridView.DataSource = dt;
        }
    }
}
