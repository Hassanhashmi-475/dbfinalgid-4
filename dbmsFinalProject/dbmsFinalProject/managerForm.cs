﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace dbmsFinalProject
{
    public partial class managerForm : Form
    {

                            [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                            private static extern IntPtr CreateRoundRectRgn
                    (
                    int nLeftRect,
                    int nTopRect,
                    int nRightRect,
                    int nBottomRect,
                    int nWidthEllipse,
                    int nHeightEllipse

                    );

        public managerForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            loginForm page = new loginForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            addEmployeesForm page = new addEmployeesForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            addDealersForm page = new addDealersForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addRawMaterialsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            addRawMaterialsForm page = new addRawMaterialsForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addAttendencesButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            addAttendencesForm page = new addAttendencesForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addSanitizationsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            addSanitizationsForm page = new addSanitizationsForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            viewReportsForm page = new viewReportsForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void managerForm_Load(object sender, EventArgs e)
        {

        }
    }
}
