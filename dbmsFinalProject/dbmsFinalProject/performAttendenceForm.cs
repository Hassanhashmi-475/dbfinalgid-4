﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;



namespace dbmsFinalProject
{
    public partial class performAttendenceForm : Form
    {


                        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                        private static extern IntPtr CreateRoundRectRgn
                (
                int nLeftRect,
                int nTopRect,
                int nRightRect,
                int nBottomRect,
                int nWidthEllipse,
                int nHeightEllipse

                );


        public performAttendenceForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            employeeForm page = new employeeForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void performAttendenceForm_Load(object sender, EventArgs e)
        {

            //Employee Get names who are added by manager
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select UserName as UName,EmployeeID as ID from Employees join users on EmployeeID=UserID where UserID NOt IN (SELECT ManagerID FROM Manager)", con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            employeeIDComboBox.DisplayMember = "UName";
            employeeIDComboBox.ValueMember = "ID";
            employeeIDComboBox.DataSource = dt;

            // Attendance Status of Employee from Lookup
            SqlCommand cmd1 = new SqlCommand("Select Value as Val,ID as Idd from Lookup where Category='Status' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da.Fill(dt1);
            statusComboBox.DisplayMember = "Val";
            statusComboBox.ValueMember = "Idd";
            statusComboBox.DataSource = dt1;


            // Get Attendance ID's
            SqlCommand cmd2 = new SqlCommand("Select AttendenceID from [Attendence] ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter daa = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            daa.Fill(dt2);
            foreach (DataRow dr in dt2.Rows)
            {

                attendenceIDComboBox.Items.Add(dr["AttendenceID"].ToString());


            }
            


        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Attendence Details] ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            attendenceDetailsDataGridView.DataSource = dt;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into [Attendence Details] values(@AttendenceID,@EmployeeID,@Status)", con);
            cmd.Parameters.AddWithValue("@AttendenceID", int.Parse(attendenceIDComboBox.Text));
            cmd.Parameters.AddWithValue("@EmployeeID", employeeIDComboBox.SelectedValue);
            cmd.Parameters.AddWithValue("@Status", statusComboBox.SelectedValue);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");

        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from [Attendence Details] where EmployeeID=@EmployeeID and AttendenceID=@AttendenceID ", con);
            cmd1.Parameters.AddWithValue("@AttendenceID", attendenceIDComboBox.Text);
            cmd1.Parameters.AddWithValue("@EmployeeID", employeeIDComboBox.SelectedValue);
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Data");
            attendenceIDComboBox.Text = "";
            employeeIDComboBox.Text = "";
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Update [Attendence Details] set  Status=@Status where  EmployeeID=@EmployeeID and AttendenceID=@AttendenceID", con);
            cmd.Parameters.AddWithValue("@AttendenceID", int.Parse(attendenceIDComboBox.Text));
            cmd.Parameters.AddWithValue("@EmployeeID", employeeIDComboBox.SelectedValue);
            cmd.Parameters.AddWithValue("@Status", statusComboBox.SelectedValue);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully updated");
        }
    }
}
