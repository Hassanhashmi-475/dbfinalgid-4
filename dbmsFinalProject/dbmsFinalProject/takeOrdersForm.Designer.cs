﻿namespace dbmsFinalProject
{
    partial class takeOrdersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.showButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.employeeIDComboBox = new System.Windows.Forms.ComboBox();
            this.OrderLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LabelShow = new System.Windows.Forms.Label();
            this.CustomerLabel = new System.Windows.Forms.Label();
            this.productIDComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.quantityTextBox = new System.Windows.Forms.TextBox();
            this.orderDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.orderDetailsDataGridView = new System.Windows.Forms.DataGridView();
            this.createOrderBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.priceBox = new System.Windows.Forms.TextBox();
            this.CustomertextBox = new System.Windows.Forms.TextBox();
            this.OrderTextBox = new System.Windows.Forms.TextBox();
            this.AddOrderBTn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.billbtn = new System.Windows.Forms.Button();
            this.kryptonPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.ProductBillDatagridview = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.billorderBox = new System.Windows.Forms.TextBox();
            this.labelBill = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.billtextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.amtLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LoadDiscountBtn = new System.Windows.Forms.Button();
            this.discountbox = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).BeginInit();
            this.kryptonPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductBillDatagridview)).BeginInit();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(880, 18);
            this.backButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(112, 35);
            this.backButton.TabIndex = 9;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // showButton
            // 
            this.showButton.Location = new System.Drawing.Point(405, 342);
            this.showButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.showButton.Name = "showButton";
            this.showButton.Size = new System.Drawing.Size(112, 35);
            this.showButton.TabIndex = 82;
            this.showButton.Text = "Show";
            this.showButton.UseVisualStyleBackColor = true;
            this.showButton.Click += new System.EventHandler(this.showButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(284, 342);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(112, 35);
            this.deleteButton.TabIndex = 81;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(162, 342);
            this.updateButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(112, 35);
            this.updateButton.TabIndex = 80;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(40, 342);
            this.addButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(112, 35);
            this.addButton.TabIndex = 79;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // employeeIDComboBox
            // 
            this.employeeIDComboBox.FormattingEnabled = true;
            this.employeeIDComboBox.Location = new System.Drawing.Point(178, 200);
            this.employeeIDComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.employeeIDComboBox.Name = "employeeIDComboBox";
            this.employeeIDComboBox.Size = new System.Drawing.Size(169, 28);
            this.employeeIDComboBox.TabIndex = 77;
            // 
            // OrderLabel
            // 
            this.OrderLabel.AutoSize = true;
            this.OrderLabel.Location = new System.Drawing.Point(36, 33);
            this.OrderLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.OrderLabel.Name = "OrderLabel";
            this.OrderLabel.Size = new System.Drawing.Size(70, 20);
            this.OrderLabel.TabIndex = 74;
            this.OrderLabel.Text = "Order ID";
            this.OrderLabel.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 205);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 20);
            this.label6.TabIndex = 75;
            this.label6.Text = "Employee ID";
            // 
            // LabelShow
            // 
            this.LabelShow.AutoSize = true;
            this.LabelShow.Location = new System.Drawing.Point(280, 18);
            this.LabelShow.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelShow.Name = "LabelShow";
            this.LabelShow.Size = new System.Drawing.Size(0, 20);
            this.LabelShow.TabIndex = 83;
            this.LabelShow.Visible = false;
            // 
            // CustomerLabel
            // 
            this.CustomerLabel.AutoSize = true;
            this.CustomerLabel.Location = new System.Drawing.Point(36, 72);
            this.CustomerLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.CustomerLabel.Name = "CustomerLabel";
            this.CustomerLabel.Size = new System.Drawing.Size(99, 20);
            this.CustomerLabel.TabIndex = 84;
            this.CustomerLabel.Text = "Customer ID";
            this.CustomerLabel.Visible = false;
            // 
            // productIDComboBox
            // 
            this.productIDComboBox.FormattingEnabled = true;
            this.productIDComboBox.Location = new System.Drawing.Point(178, 242);
            this.productIDComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.productIDComboBox.Name = "productIDComboBox";
            this.productIDComboBox.Size = new System.Drawing.Size(169, 28);
            this.productIDComboBox.TabIndex = 87;
            this.productIDComboBox.SelectedIndexChanged += new System.EventHandler(this.productIDComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 246);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 20);
            this.label4.TabIndex = 86;
            this.label4.Text = "Product ID";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(400, 162);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 20);
            this.label5.TabIndex = 89;
            this.label5.Text = "Phone";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(400, 128);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 20);
            this.label7.TabIndex = 88;
            this.label7.Text = "Address";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(400, 200);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 20);
            this.label10.TabIndex = 90;
            this.label10.Text = "Quantity";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(400, 283);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(88, 20);
            this.label9.TabIndex = 91;
            this.label9.Text = "Order Date";
            // 
            // addressTextBox
            // 
            this.addressTextBox.Location = new System.Drawing.Point(495, 117);
            this.addressTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(169, 26);
            this.addressTextBox.TabIndex = 92;
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.Location = new System.Drawing.Point(495, 157);
            this.phoneTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(169, 26);
            this.phoneTextBox.TabIndex = 93;
            // 
            // quantityTextBox
            // 
            this.quantityTextBox.Location = new System.Drawing.Point(495, 197);
            this.quantityTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.quantityTextBox.Name = "quantityTextBox";
            this.quantityTextBox.Size = new System.Drawing.Size(169, 26);
            this.quantityTextBox.TabIndex = 94;
            // 
            // orderDateDateTimePicker
            // 
            this.orderDateDateTimePicker.Location = new System.Drawing.Point(495, 283);
            this.orderDateDateTimePicker.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderDateDateTimePicker.Name = "orderDateDateTimePicker";
            this.orderDateDateTimePicker.Size = new System.Drawing.Size(169, 26);
            this.orderDateDateTimePicker.TabIndex = 95;
            // 
            // orderDetailsDataGridView
            // 
            this.orderDetailsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderDetailsDataGridView.Location = new System.Drawing.Point(659, 386);
            this.orderDetailsDataGridView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.orderDetailsDataGridView.Name = "orderDetailsDataGridView";
            this.orderDetailsDataGridView.RowHeadersWidth = 62;
            this.orderDetailsDataGridView.Size = new System.Drawing.Size(551, 255);
            this.orderDetailsDataGridView.TabIndex = 96;
            // 
            // createOrderBtn
            // 
            this.createOrderBtn.Location = new System.Drawing.Point(404, 14);
            this.createOrderBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.createOrderBtn.Name = "createOrderBtn";
            this.createOrderBtn.Size = new System.Drawing.Size(181, 35);
            this.createOrderBtn.TabIndex = 100;
            this.createOrderBtn.Text = "Create New Order";
            this.createOrderBtn.UseVisualStyleBackColor = true;
            this.createOrderBtn.Visible = false;
            this.createOrderBtn.Click += new System.EventHandler(this.createOrderBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(401, 242);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 20);
            this.label1.TabIndex = 101;
            this.label1.Text = "Price";
            // 
            // priceBox
            // 
            this.priceBox.Location = new System.Drawing.Point(495, 236);
            this.priceBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.priceBox.Name = "priceBox";
            this.priceBox.Size = new System.Drawing.Size(169, 26);
            this.priceBox.TabIndex = 102;
            // 
            // CustomertextBox
            // 
            this.CustomertextBox.Location = new System.Drawing.Point(178, 66);
            this.CustomertextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CustomertextBox.Multiline = true;
            this.CustomertextBox.Name = "CustomertextBox";
            this.CustomertextBox.Size = new System.Drawing.Size(169, 26);
            this.CustomertextBox.TabIndex = 103;
            this.CustomertextBox.Visible = false;
            // 
            // OrderTextBox
            // 
            this.OrderTextBox.Location = new System.Drawing.Point(178, 27);
            this.OrderTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.OrderTextBox.Multiline = true;
            this.OrderTextBox.Name = "OrderTextBox";
            this.OrderTextBox.Size = new System.Drawing.Size(169, 26);
            this.OrderTextBox.TabIndex = 104;
            this.OrderTextBox.Visible = false;
            // 
            // AddOrderBTn
            // 
            this.AddOrderBTn.Location = new System.Drawing.Point(404, 59);
            this.AddOrderBTn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.AddOrderBTn.Name = "AddOrderBTn";
            this.AddOrderBTn.Size = new System.Drawing.Size(181, 35);
            this.AddOrderBTn.TabIndex = 105;
            this.AddOrderBTn.Text = "Add Item";
            this.AddOrderBTn.UseVisualStyleBackColor = true;
            this.AddOrderBTn.Visible = false;
            this.AddOrderBTn.Click += new System.EventHandler(this.AddOrderBTn_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(622, 14);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(181, 35);
            this.button1.TabIndex = 106;
            this.button1.Text = "Finish Order";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // billbtn
            // 
            this.billbtn.Location = new System.Drawing.Point(622, 59);
            this.billbtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.billbtn.Name = "billbtn";
            this.billbtn.Size = new System.Drawing.Size(181, 35);
            this.billbtn.TabIndex = 107;
            this.billbtn.Text = "Generate Bill";
            this.billbtn.UseVisualStyleBackColor = true;
            this.billbtn.Click += new System.EventHandler(this.billbtn_Click);
            // 
            // kryptonPanel1
            // 
            this.kryptonPanel1.Controls.Add(this.ProductBillDatagridview);
            this.kryptonPanel1.Controls.Add(this.label3);
            this.kryptonPanel1.Controls.Add(this.billorderBox);
            this.kryptonPanel1.Controls.Add(this.labelBill);
            this.kryptonPanel1.Controls.Add(this.billtextBox);
            this.kryptonPanel1.Controls.Add(this.label2);
            this.kryptonPanel1.Controls.Add(this.amtLabel);
            this.kryptonPanel1.Location = new System.Drawing.Point(12, 389);
            this.kryptonPanel1.Name = "kryptonPanel1";
            this.kryptonPanel1.Size = new System.Drawing.Size(616, 259);
            this.kryptonPanel1.TabIndex = 108;
            // 
            // ProductBillDatagridview
            // 
            this.ProductBillDatagridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProductBillDatagridview.Location = new System.Drawing.Point(8, 38);
            this.ProductBillDatagridview.Name = "ProductBillDatagridview";
            this.ProductBillDatagridview.RowHeadersWidth = 62;
            this.ProductBillDatagridview.RowTemplate.Height = 28;
            this.ProductBillDatagridview.Size = new System.Drawing.Size(605, 164);
            this.ProductBillDatagridview.TabIndex = 109;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 12);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 20);
            this.label3.TabIndex = 114;
            this.label3.Text = "Order ID";
            // 
            // billorderBox
            // 
            this.billorderBox.Location = new System.Drawing.Point(98, 9);
            this.billorderBox.Name = "billorderBox";
            this.billorderBox.Size = new System.Drawing.Size(68, 26);
            this.billorderBox.TabIndex = 113;
            // 
            // labelBill
            // 
            this.labelBill.Location = new System.Drawing.Point(3, 55);
            this.labelBill.Name = "labelBill";
            this.labelBill.Size = new System.Drawing.Size(6, 2);
            this.labelBill.TabIndex = 112;
            this.labelBill.Values.Text = "";
            // 
            // billtextBox
            // 
            this.billtextBox.Location = new System.Drawing.Point(173, 226);
            this.billtextBox.Name = "billtextBox";
            this.billtextBox.Size = new System.Drawing.Size(100, 26);
            this.billtextBox.TabIndex = 111;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(137, 232);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 20);
            this.label2.TabIndex = 110;
            this.label2.Text = "Rs";
            // 
            // amtLabel
            // 
            this.amtLabel.AutoSize = true;
            this.amtLabel.Location = new System.Drawing.Point(4, 232);
            this.amtLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.amtLabel.Name = "amtLabel";
            this.amtLabel.Size = new System.Drawing.Size(116, 20);
            this.amtLabel.TabIndex = 109;
            this.amtLabel.Text = "Total Amount : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(685, 163);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(130, 20);
            this.label8.TabIndex = 110;
            this.label8.Text = "Dicount(optional)";
            // 
            // LoadDiscountBtn
            // 
            this.LoadDiscountBtn.Location = new System.Drawing.Point(823, 200);
            this.LoadDiscountBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.LoadDiscountBtn.Name = "LoadDiscountBtn";
            this.LoadDiscountBtn.Size = new System.Drawing.Size(169, 35);
            this.LoadDiscountBtn.TabIndex = 111;
            this.LoadDiscountBtn.Text = "Load Discount ";
            this.LoadDiscountBtn.UseVisualStyleBackColor = true;
            this.LoadDiscountBtn.Click += new System.EventHandler(this.LoadDiscountBtn_Click);
            // 
            // discountbox
            // 
            this.discountbox.FormattingEnabled = true;
            this.discountbox.Location = new System.Drawing.Point(823, 159);
            this.discountbox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.discountbox.Name = "discountbox";
            this.discountbox.Size = new System.Drawing.Size(169, 28);
            this.discountbox.TabIndex = 112;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(845, 250);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 20);
            this.label11.TabIndex = 113;
            // 
            // takeOrdersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1223, 737);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.discountbox);
            this.Controls.Add(this.LoadDiscountBtn);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.kryptonPanel1);
            this.Controls.Add(this.billbtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.AddOrderBTn);
            this.Controls.Add(this.OrderTextBox);
            this.Controls.Add(this.CustomertextBox);
            this.Controls.Add(this.priceBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.createOrderBtn);
            this.Controls.Add(this.orderDetailsDataGridView);
            this.Controls.Add(this.orderDateDateTimePicker);
            this.Controls.Add(this.quantityTextBox);
            this.Controls.Add(this.phoneTextBox);
            this.Controls.Add(this.addressTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.productIDComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.CustomerLabel);
            this.Controls.Add(this.LabelShow);
            this.Controls.Add(this.showButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.employeeIDComboBox);
            this.Controls.Add(this.OrderLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.backButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "takeOrdersForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "S";
            this.Load += new System.EventHandler(this.takeOrdersForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.orderDetailsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonPanel1)).EndInit();
            this.kryptonPanel1.ResumeLayout(false);
            this.kryptonPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProductBillDatagridview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button showButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.ComboBox employeeIDComboBox;
        private System.Windows.Forms.Label OrderLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LabelShow;
        private System.Windows.Forms.Label CustomerLabel;
        private System.Windows.Forms.ComboBox productIDComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.TextBox quantityTextBox;
        private System.Windows.Forms.DateTimePicker orderDateDateTimePicker;
        private System.Windows.Forms.DataGridView orderDetailsDataGridView;
        private System.Windows.Forms.Button createOrderBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox priceBox;
        private System.Windows.Forms.TextBox CustomertextBox;
        private System.Windows.Forms.TextBox OrderTextBox;
        private System.Windows.Forms.Button AddOrderBTn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button billbtn;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel kryptonPanel1;
        private System.Windows.Forms.TextBox billtextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label amtLabel;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel labelBill;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox billorderBox;
        private System.Windows.Forms.DataGridView ProductBillDatagridview;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button LoadDiscountBtn;
        private System.Windows.Forms.ComboBox discountbox;
        private System.Windows.Forms.Label label11;
    }
}