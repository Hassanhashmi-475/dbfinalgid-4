﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;



namespace dbmsFinalProject
{
    public partial class addDiscountsForm : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

        private static extern IntPtr CreateRoundRectRgn
       (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
               int nHeightEllipse

        );
        public addDiscountsForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            employeeForm page = new employeeForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Discounts] ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            discountsDataGridView.DataSource = dt;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(discountValueTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in Discount Value!");
                discountValueTextBox.Text = discountValueTextBox.Text.Remove(discountValueTextBox.Text.Length - 1);

            }
            else if(discountValueTextBox.Text=="" || DiscountNameTextBox.Text == "") 
            { MessageBox.Show("Please fill all the fields"); }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into [Discounts] values(@DiscountName,@DiscountValue)", con);
                cmd.Parameters.AddWithValue("@DiscountName", DiscountNameTextBox.Text);
                cmd.Parameters.AddWithValue("@DiscountValue", float.Parse(discountValueTextBox.Text));
                // cmd.Parameters.Add("[@Expiry Date]", SqlDbType.DateTime).Value = expiryDateDateTimePicker.Text;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(discountValueTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in Discount Value!");
                discountValueTextBox.Text = discountValueTextBox.Text.Remove(discountValueTextBox.Text.Length - 1);

            }
            else if (discountValueTextBox.Text == "" || DiscountNameTextBox.Text == "")
            { MessageBox.Show("Please fill all the fields"); }

            else
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Discounts set [Discount Value]=@DiscountValue where DiscountName=@DiscountName", con);
                cmd.Parameters.AddWithValue("@DiscountName", DiscountNameTextBox.Text);
                cmd.Parameters.AddWithValue("@DiscountValue", float.Parse(discountValueTextBox.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Update");
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from [Discounts] where DiscountName=@DiscountName", con);
            cmd1.Parameters.AddWithValue("@DiscountName", DiscountNameTextBox.Text);
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Data");
            DiscountNameTextBox.Clear();
        }

        private void DiscountNameTextBox_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select [Discount Value] from [Discounts] where DiscountName=@DiscountName", con);
            cmd.Parameters.AddWithValue("@DiscountName",DiscountNameTextBox.Text);

            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {

                discountValueTextBox.Text = da.GetValue(0).ToString();



            }
            if (DiscountNameTextBox.Text == "" )
            {

                discountValueTextBox.Text = "";

            }
        }

        private void addDiscountsForm_Load(object sender, EventArgs e)
        {

        }
    }
}
