﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;


namespace dbmsFinalProject
{
    public partial class addRawMaterialsForm : Form
    {

                        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                        private static extern IntPtr CreateRoundRectRgn
                (
                int nLeftRect,
                int nTopRect,
                int nRightRect,
                int nBottomRect,
                int nWidthEllipse,
                int nHeightEllipse

                );


        public addRawMaterialsForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }


        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            managerForm page = new managerForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addRawMaterialsForm_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select D.CompanyName as CompmanyName,D.DealerID as ID from Dealers D",con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dealerIDComboBox.DisplayMember = "CompmanyName";
            dealerIDComboBox.ValueMember = "ID";
            dealerIDComboBox.DataSource = dt;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (dealerIDComboBox.Text == "" || textBox1.Text == ""  || expiryDateDateTimePicker.Text == "" || textBox3.Text == "")
            {
                MessageBox.Show("Please fill the all  fields");
            }
           
            else if (System.Text.RegularExpressions.Regex.IsMatch(textBox1.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter valid unit in stock");
                textBox1.Text = textBox1.Text.Remove(textBox1.Text.Length - 1);

            }

            else if (System.Text.RegularExpressions.Regex.IsMatch(textBox3.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter valid number in raw id");
                textBox3.Text = textBox3.Text.Remove(textBox3.Text.Length - 1);

            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into [Raw_Materials] values(@Raw_MaterialID,@DealerID,@Units_in_Stock,@ExpiryDate)", con);
                cmd.Parameters.AddWithValue("@Raw_MaterialID", int.Parse(textBox3.Text));
                cmd.Parameters.AddWithValue("@DealerID", dealerIDComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@Units_in_Stock", int.Parse(textBox1.Text));
                cmd.Parameters.AddWithValue("@ExpiryDate", DateTime.Parse(expiryDateDateTimePicker.Text));
               // cmd.Parameters.Add("[@Expiry Date]", SqlDbType.DateTime).Value = expiryDateDateTimePicker.Text;
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Raw_Materials] ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            rawMaterialsDataGridView.DataSource = dt;
        }

        private void updateButton_Click(object sender, EventArgs e)
        {



            if (dealerIDComboBox.Text == "" || textBox1.Text == "" ||  expiryDateDateTimePicker.Text == "" || textBox3.Text=="")
            {
                MessageBox.Show("Please fill the all  fields");
            }

            else if (System.Text.RegularExpressions.Regex.IsMatch(textBox1.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in Stock ");
                textBox1.Text = textBox1.Text.Remove(textBox1.Text.Length - 1);

            }

            else if (System.Text.RegularExpressions.Regex.IsMatch(textBox3.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter valid number in raw id");
                textBox3.Text = textBox3.Text.Remove(textBox3.Text.Length - 1);

            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update [Raw_Materials] set DealerID=@DealerID,Units_in_Stock=@Units_in_Stock,[Expiry Date]=@ExpiryDate where Raw_MaterialID=@Raw_MaterialID", con);

                cmd.Parameters.AddWithValue("@Raw_MaterialID", int.Parse(textBox3.Text));
                cmd.Parameters.AddWithValue("@DealerID", dealerIDComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@Units_in_Stock", int.Parse(textBox1.Text));
                cmd.Parameters.AddWithValue("@ExpiryDate", DateTime.Parse(expiryDateDateTimePicker.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully updated");
            }
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select DealerID ,Units_in_Stock,[Expiry Date] from [Raw_Materials] where Raw_MaterialID=@Raw_MaterialID", con);
            cmd.Parameters.AddWithValue("@Raw_MaterialID", textBox3.Text);

            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {

                dealerIDComboBox.Text = da.GetValue(0).ToString();
                textBox1.Text = da.GetValue(1).ToString();
                expiryDateDateTimePicker.Text = da.GetValue(3).ToString();


            }
            if (textBox3.Text == "")
            {

                dealerIDComboBox.Text = "";
                textBox1.Clear();
                expiryDateDateTimePicker.ResetText();

            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from [Raw_Materials] where Raw_MaterialID=@Raw_MaterialID ", con);
            cmd1.Parameters.AddWithValue("@Raw_MaterialID", textBox3.Text);
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Data");
            textBox3.Clear();
        }

        private void expiryDelete_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from [Raw_Materials] where    GETDATE() >   [Expiry Date]  ", con); 
            SqlCommand cmd2 = new SqlCommand("Delete from [Recipie Details] where IngredientID in ( Select Raw_MaterialID from Raw_Materials where GETDATE() >   [Expiry Date] ) ", con);


            cmd2.ExecuteNonQuery();

            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Expired Raw Materials");
            textBox3.Clear();
        }
    }
}
