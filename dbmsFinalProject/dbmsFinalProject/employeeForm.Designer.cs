﻿namespace dbmsFinalProject
{
    partial class employeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.loginPanel = new System.Windows.Forms.Panel();
            this.generateBillButton = new System.Windows.Forms.Button();
            this.performAttendenceButton = new System.Windows.Forms.Button();
            this.takeOrdersButton = new System.Windows.Forms.Button();
            this.addDiscountsButton = new System.Windows.Forms.Button();
            this.addRecipieDetailsButton = new System.Windows.Forms.Button();
            this.addProductssButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.loginPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(732, 18);
            this.backButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(112, 35);
            this.backButton.TabIndex = 8;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // loginPanel
            // 
            this.loginPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.loginPanel.Controls.Add(this.generateBillButton);
            this.loginPanel.Controls.Add(this.performAttendenceButton);
            this.loginPanel.Controls.Add(this.takeOrdersButton);
            this.loginPanel.Controls.Add(this.addDiscountsButton);
            this.loginPanel.Controls.Add(this.addRecipieDetailsButton);
            this.loginPanel.Controls.Add(this.addProductssButton);
            this.loginPanel.Location = new System.Drawing.Point(0, 0);
            this.loginPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loginPanel.Name = "loginPanel";
            this.loginPanel.Size = new System.Drawing.Size(240, 488);
            this.loginPanel.TabIndex = 9;
            // 
            // generateBillButton
            // 
            this.generateBillButton.Location = new System.Drawing.Point(32, 358);
            this.generateBillButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.generateBillButton.Name = "generateBillButton";
            this.generateBillButton.Size = new System.Drawing.Size(186, 45);
            this.generateBillButton.TabIndex = 14;
            this.generateBillButton.Text = "Generate BIll";
            this.generateBillButton.UseVisualStyleBackColor = true;
            this.generateBillButton.Click += new System.EventHandler(this.generateBillButton_Click);
            // 
            // performAttendenceButton
            // 
            this.performAttendenceButton.Location = new System.Drawing.Point(32, 305);
            this.performAttendenceButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.performAttendenceButton.Name = "performAttendenceButton";
            this.performAttendenceButton.Size = new System.Drawing.Size(186, 45);
            this.performAttendenceButton.TabIndex = 13;
            this.performAttendenceButton.Text = "Perform Attendence";
            this.performAttendenceButton.UseVisualStyleBackColor = true;
            this.performAttendenceButton.Click += new System.EventHandler(this.performAttendenceButton_Click);
            // 
            // takeOrdersButton
            // 
            this.takeOrdersButton.Location = new System.Drawing.Point(32, 251);
            this.takeOrdersButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.takeOrdersButton.Name = "takeOrdersButton";
            this.takeOrdersButton.Size = new System.Drawing.Size(186, 45);
            this.takeOrdersButton.TabIndex = 11;
            this.takeOrdersButton.Text = "Take Orders";
            this.takeOrdersButton.UseVisualStyleBackColor = true;
            this.takeOrdersButton.Click += new System.EventHandler(this.takeOrdersButton_Click);
            // 
            // addDiscountsButton
            // 
            this.addDiscountsButton.Location = new System.Drawing.Point(32, 197);
            this.addDiscountsButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addDiscountsButton.Name = "addDiscountsButton";
            this.addDiscountsButton.Size = new System.Drawing.Size(186, 45);
            this.addDiscountsButton.TabIndex = 10;
            this.addDiscountsButton.Text = "Add Discounts";
            this.addDiscountsButton.UseVisualStyleBackColor = true;
            this.addDiscountsButton.Click += new System.EventHandler(this.addDiscountsButton_Click);
            // 
            // addRecipieDetailsButton
            // 
            this.addRecipieDetailsButton.Location = new System.Drawing.Point(32, 143);
            this.addRecipieDetailsButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addRecipieDetailsButton.Name = "addRecipieDetailsButton";
            this.addRecipieDetailsButton.Size = new System.Drawing.Size(186, 45);
            this.addRecipieDetailsButton.TabIndex = 10;
            this.addRecipieDetailsButton.Text = "Add Recipie Details";
            this.addRecipieDetailsButton.UseVisualStyleBackColor = true;
            this.addRecipieDetailsButton.Click += new System.EventHandler(this.addRecipieDetailsButton_Click);
            // 
            // addProductssButton
            // 
            this.addProductssButton.Location = new System.Drawing.Point(32, 89);
            this.addProductssButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addProductssButton.Name = "addProductssButton";
            this.addProductssButton.Size = new System.Drawing.Size(186, 45);
            this.addProductssButton.TabIndex = 9;
            this.addProductssButton.Text = "Add Products";
            this.addProductssButton.UseVisualStyleBackColor = true;
            this.addProductssButton.Click += new System.EventHandler(this.addProductssButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(582, 305);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 29);
            this.label4.TabIndex = 14;
            this.label4.Text = "EMPLOYEE";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(380, 200);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 29);
            this.label2.TabIndex = 12;
            this.label2.Text = "WELCOME";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(494, 251);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 29);
            this.label3.TabIndex = 13;
            this.label3.Text = "BAKERY";
            // 
            // employeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 635);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.loginPanel);
            this.Controls.Add(this.backButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "employeeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Employee Form";
            this.Load += new System.EventHandler(this.employeeForm_Load);
            this.loginPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Panel loginPanel;
        private System.Windows.Forms.Button generateBillButton;
        private System.Windows.Forms.Button performAttendenceButton;
        private System.Windows.Forms.Button takeOrdersButton;
        private System.Windows.Forms.Button addDiscountsButton;
        private System.Windows.Forms.Button addRecipieDetailsButton;
        private System.Windows.Forms.Button addProductssButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}