﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;


namespace dbmsFinalProject
{
    public partial class addDealersForm : Form
    {


                            [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                            private static extern IntPtr CreateRoundRectRgn
                    (
                         int nLeftRect,
                         int nTopRect,
                         int nRightRect,
                         int nBottomRect,
                         int nWidthEllipse,
                            int nHeightEllipse

                     );

        public addDealersForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));


        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            managerForm page = new managerForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            if (companyNameTextBox.Text == "") { MessageBox.Show("Enter the Company Name"); }
            else if (System.Text.RegularExpressions.Regex.IsMatch(phoneTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in phone");
                phoneTextBox.Text = phoneTextBox.Text.Remove(phoneTextBox.Text.Length - 1);

            }
            else if (phoneTextBox.Text.Length < 11 || phoneTextBox.Text.Length > 11) { MessageBox.Show("Phone must be of 11 Numbers"); }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Dealers values(@CompanyName,@WareHouseAddress,@City,@Phone)", con);
                cmd.Parameters.AddWithValue("@CompanyName", (companyNameTextBox.Text));
                cmd.Parameters.AddWithValue("@WareHouseAddress", wareHouseAddressTextBox.Text);
                cmd.Parameters.AddWithValue("@City", (cityTextBox.Text));
                cmd.Parameters.AddWithValue("@Phone", phoneTextBox.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Dealers", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dealersDataGridView.DataSource = dt;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from Dealers where CompanyName=@CompanyName ", con);
            cmd1.Parameters.AddWithValue("@CompanyName", companyNameTextBox.Text);
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Data");
            companyNameTextBox.Clear();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {

            if (System.Text.RegularExpressions.Regex.IsMatch(phoneTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in phone");
                phoneTextBox.Text = phoneTextBox.Text.Remove(phoneTextBox.Text.Length - 1);

            }
            else if (phoneTextBox.Text.Length < 11 || phoneTextBox.Text.Length > 11) { MessageBox.Show("Phone must be of 11 Numbers"); }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update [Dealers] set WareHouseAddress=@WareHouseAddress,City=@City,@Phone=Phone where CompanyName=@CompanyName", con);
                cmd.Parameters.AddWithValue("@CompanyName", (companyNameTextBox.Text));
                cmd.Parameters.AddWithValue("@WareHouseAddress", wareHouseAddressTextBox.Text);
                cmd.Parameters.AddWithValue("@City", (cityTextBox.Text));
                cmd.Parameters.AddWithValue("@Phone", phoneTextBox.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");
            }
        }

        private void companyNameTextBox_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select  WareHouseAddress ,City,Phone from [Dealers] where CompanyName=@CompanyName", con);
            cmd.Parameters.AddWithValue("@CompanyName",companyNameTextBox.Text);

            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {

                wareHouseAddressTextBox.Text = da.GetValue(0).ToString();
                cityTextBox.Text = da.GetValue(1).ToString();
                phoneTextBox.Text = da.GetValue(2).ToString();





            }
            if (companyNameTextBox.Text == "")
            {

                wareHouseAddressTextBox.Clear();
                cityTextBox.Clear();
                phoneTextBox.Clear();

            }
        }
    }
}
