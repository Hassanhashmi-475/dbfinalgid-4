﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;


namespace dbmsFinalProject
{
    public partial class addAttendencesForm : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                        private static extern IntPtr CreateRoundRectRgn
                (
                     int nLeftRect,
                     int nTopRect,
                     int nRightRect,
                     int nBottomRect,
                     int nWidthEllipse,
                        int nHeightEllipse

                 );
        


        public addAttendencesForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            managerForm page = new managerForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (dateCreatedDateTimePicker.Text == "") { MessageBox.Show("Please fill the date"); }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into [Attendence] values(@DateCreated)", con);
                cmd.Parameters.AddWithValue("@DateCreated", DateTime.Parse(dateCreatedDateTimePicker.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Attendence] ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            attendencesDataGridView.DataSource = dt;
        }
    }
}
