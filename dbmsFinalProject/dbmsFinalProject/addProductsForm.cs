﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;




namespace dbmsFinalProject
{
    public partial class addProductsForm : Form
    {



                    [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                    private static extern IntPtr CreateRoundRectRgn
            (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse

            );
        public addProductsForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            employeeForm page = new employeeForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            if (productNameTextBox.Text == "")
            {
                MessageBox.Show("Please Enter the product name");
            }
            else if (discontinuedTextBox.Text == "") { MessageBox.Show("Please Enter the discontinued field box"); }
            else if (expiryDateDateTimePicker.Text == "") { MessageBox.Show("Enter Date "); }

            else if (System.Text.RegularExpressions.Regex.IsMatch(unitsInStockTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in Stock!");
                unitsInStockTextBox.Text = unitsInStockTextBox.Text.Remove(unitsInStockTextBox.Text.Length - 1);

            }

            else if (System.Text.RegularExpressions.Regex.IsMatch(unitPriceTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in price !");
                unitPriceTextBox.Text = unitPriceTextBox.Text.Remove(unitPriceTextBox.Text.Length - 1);

            }

            else
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into [Products] values(@ProductName,@CategoryID,@UnitPrice,@UnitsInStock,@Discontinued,@ExpiryDate)", con);
                cmd.Parameters.AddWithValue("@ProductName", productNameTextBox.Text);
                cmd.Parameters.AddWithValue("@CategoryID", categoryIDComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@UnitPrice", int.Parse(unitPriceTextBox.Text));
                cmd.Parameters.AddWithValue("@UnitsInStock", int.Parse(unitsInStockTextBox.Text));
                cmd.Parameters.AddWithValue("@Discontinued", discontinuedTextBox.Text);
                cmd.Parameters.AddWithValue("@ExpiryDate", DateTime.Parse(expiryDateDateTimePicker.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Saved");
            }
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Products] ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            productsDataGridView.DataSource = dt;
        }

        private void addProductsForm_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Value as val,ID as Id from Lookup where Category='Category'", con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            categoryIDComboBox.DisplayMember = "Val";
            categoryIDComboBox.ValueMember = "Id";
            categoryIDComboBox.DataSource = dt;
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            if (productNameTextBox.Text == "")
            {
                MessageBox.Show("Please Enter the product name");
            }
            else if (discontinuedTextBox.Text == "") { MessageBox.Show("Please Enter the discontinued field box"); }
            else if (expiryDateDateTimePicker.Text == "") { MessageBox.Show("Enter Date "); }

            else if (System.Text.RegularExpressions.Regex.IsMatch(unitsInStockTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in Stock!");
                unitsInStockTextBox.Text = unitsInStockTextBox.Text.Remove(unitsInStockTextBox.Text.Length - 1);

            }

            else if (System.Text.RegularExpressions.Regex.IsMatch(unitPriceTextBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in price !");
                unitPriceTextBox.Text = unitPriceTextBox.Text.Remove(unitPriceTextBox.Text.Length - 1);

            }

            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update [Products] set CategoryID=@CategoryID,UnitPrice=@UnitPrice,UnitsInStock=@UnitsInStock,Discontinued=@Discontinued,[Expiry Date]=@ExpiryDate where ProductName=@ProductName", con);
                cmd.Parameters.AddWithValue("@ProductName", productNameTextBox.Text);
                cmd.Parameters.AddWithValue("@CategoryID", categoryIDComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@UnitPrice", int.Parse(unitPriceTextBox.Text));
                cmd.Parameters.AddWithValue("@UnitsInStock", int.Parse(unitsInStockTextBox.Text));
                cmd.Parameters.AddWithValue("@Discontinued", discontinuedTextBox.Text);
                cmd.Parameters.AddWithValue("@ExpiryDate", DateTime.Parse(expiryDateDateTimePicker.Text));
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully updated");
            }

        }

        private void productNameTextBox_TextChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select CategoryID,UnitPrice,UnitsInStock,Discontinued,[Expiry Date] from [Products] where ProductName=@ProductName", con);
            cmd.Parameters.AddWithValue("@ProductName", productNameTextBox.Text);

            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {

                categoryIDComboBox.SelectedValue = da.GetValue(0).ToString();
                unitPriceTextBox.Text = da.GetValue(1).ToString();
                unitsInStockTextBox.Text = da.GetValue(2).ToString();
                discontinuedTextBox.Text = da.GetValue(3).ToString();
                expiryDateDateTimePicker.Text = da.GetValue(4).ToString();


            }
            if (productNameTextBox.Text == "")
            {

                
                unitPriceTextBox.Clear();
                unitsInStockTextBox.Clear();
                discontinuedTextBox.Text = "";
                expiryDateDateTimePicker.ResetText();

            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from Products where ProductName=@ProductName ",con);
            cmd1.Parameters.AddWithValue("@ProductName", productNameTextBox.Text);
            cmd1.ExecuteNonQuery();
            
            MessageBox.Show("Successfully Deleted Data");
           productNameTextBox.Clear();
        }

        private void expiryDelete_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from [Products] where    GETDATE() >   [Expiry Date]  ", con);
            SqlCommand cmd2 = new SqlCommand("Delete from [Order Details] where ProductID in ( Select ProductID from Products where GETDATE() >   [Expiry Date] ) ", con);
            SqlCommand cmd3 = new SqlCommand("Delete from [Recipie Details] where ProductID in ( Select ProductID from Products where GETDATE() >   [Expiry Date] ) ", con);


            cmd3.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();

            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Expired Products");
        }
    }
}
