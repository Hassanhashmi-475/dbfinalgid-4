﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace dbmsFinalProject
{
    public partial class viewReportsForm : Form
    {

                        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                        private static extern IntPtr CreateRoundRectRgn
                (
                int nLeftRect,
                int nTopRect,
                int nRightRect,
                int nBottomRect,
                int nWidthEllipse,
                int nHeightEllipse

                );

        public viewReportsForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            managerForm page = new managerForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }
    }
}
