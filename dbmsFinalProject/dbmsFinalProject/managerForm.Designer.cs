﻿namespace dbmsFinalProject
{
    partial class managerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backButton = new System.Windows.Forms.Button();
            this.loginPanel = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.addSanitizationsButton = new System.Windows.Forms.Button();
            this.addAttendencesButton = new System.Windows.Forms.Button();
            this.addRawMaterialsButton = new System.Windows.Forms.Button();
            this.addDealersButton = new System.Windows.Forms.Button();
            this.addEmployeesButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.loginPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(732, 18);
            this.backButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(112, 35);
            this.backButton.TabIndex = 7;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // loginPanel
            // 
            this.loginPanel.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.loginPanel.Controls.Add(this.button1);
            this.loginPanel.Controls.Add(this.addSanitizationsButton);
            this.loginPanel.Controls.Add(this.addAttendencesButton);
            this.loginPanel.Controls.Add(this.addRawMaterialsButton);
            this.loginPanel.Controls.Add(this.addDealersButton);
            this.loginPanel.Controls.Add(this.addEmployeesButton);
            this.loginPanel.Location = new System.Drawing.Point(0, 0);
            this.loginPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.loginPanel.Name = "loginPanel";
            this.loginPanel.Size = new System.Drawing.Size(240, 488);
            this.loginPanel.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(32, 358);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(186, 45);
            this.button1.TabIndex = 13;
            this.button1.Text = "View Reports";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // addSanitizationsButton
            // 
            this.addSanitizationsButton.Location = new System.Drawing.Point(32, 305);
            this.addSanitizationsButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addSanitizationsButton.Name = "addSanitizationsButton";
            this.addSanitizationsButton.Size = new System.Drawing.Size(186, 45);
            this.addSanitizationsButton.TabIndex = 12;
            this.addSanitizationsButton.Text = "Add Sanitization";
            this.addSanitizationsButton.UseVisualStyleBackColor = true;
            this.addSanitizationsButton.Click += new System.EventHandler(this.addSanitizationsButton_Click);
            // 
            // addAttendencesButton
            // 
            this.addAttendencesButton.Location = new System.Drawing.Point(32, 251);
            this.addAttendencesButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addAttendencesButton.Name = "addAttendencesButton";
            this.addAttendencesButton.Size = new System.Drawing.Size(186, 45);
            this.addAttendencesButton.TabIndex = 11;
            this.addAttendencesButton.Text = "Add Attendence";
            this.addAttendencesButton.UseVisualStyleBackColor = true;
            this.addAttendencesButton.Click += new System.EventHandler(this.addAttendencesButton_Click);
            // 
            // addRawMaterialsButton
            // 
            this.addRawMaterialsButton.Location = new System.Drawing.Point(32, 197);
            this.addRawMaterialsButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addRawMaterialsButton.Name = "addRawMaterialsButton";
            this.addRawMaterialsButton.Size = new System.Drawing.Size(186, 45);
            this.addRawMaterialsButton.TabIndex = 10;
            this.addRawMaterialsButton.Text = "Add Raw Materials";
            this.addRawMaterialsButton.UseVisualStyleBackColor = true;
            this.addRawMaterialsButton.Click += new System.EventHandler(this.addRawMaterialsButton_Click);
            // 
            // addDealersButton
            // 
            this.addDealersButton.Location = new System.Drawing.Point(32, 143);
            this.addDealersButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addDealersButton.Name = "addDealersButton";
            this.addDealersButton.Size = new System.Drawing.Size(186, 45);
            this.addDealersButton.TabIndex = 10;
            this.addDealersButton.Text = "Add Dealers";
            this.addDealersButton.UseVisualStyleBackColor = true;
            this.addDealersButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // addEmployeesButton
            // 
            this.addEmployeesButton.Location = new System.Drawing.Point(32, 89);
            this.addEmployeesButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.addEmployeesButton.Name = "addEmployeesButton";
            this.addEmployeesButton.Size = new System.Drawing.Size(186, 45);
            this.addEmployeesButton.TabIndex = 9;
            this.addEmployeesButton.Text = "Add Employees";
            this.addEmployeesButton.UseVisualStyleBackColor = true;
            this.addEmployeesButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(564, 291);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 29);
            this.label4.TabIndex = 11;
            this.label4.Text = "MANAGER";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(358, 182);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 29);
            this.label2.TabIndex = 9;
            this.label2.Text = "WELCOME";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(472, 232);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 29);
            this.label3.TabIndex = 10;
            this.label3.Text = "BAKERY";
            // 
            // managerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 635);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.loginPanel);
            this.Controls.Add(this.backButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "managerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Manager Form";
            this.Load += new System.EventHandler(this.managerForm_Load);
            this.loginPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Panel loginPanel;
        private System.Windows.Forms.Button addEmployeesButton;
        private System.Windows.Forms.Button addSanitizationsButton;
        private System.Windows.Forms.Button addAttendencesButton;
        private System.Windows.Forms.Button addRawMaterialsButton;
        private System.Windows.Forms.Button addDealersButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
    }
}