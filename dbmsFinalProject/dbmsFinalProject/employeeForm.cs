﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace dbmsFinalProject
{
    public partial class employeeForm : Form
    {

                    [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                    private static extern IntPtr CreateRoundRectRgn
            (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse

            );

        public employeeForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            loginForm page = new loginForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void employeeForm_Load(object sender, EventArgs e)
        {

        }

        private void addProductssButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            addProductsForm page = new addProductsForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addRecipieDetailsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            addRecipieDetailsForm page = new addRecipieDetailsForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addDiscountsButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            addDiscountsForm page = new addDiscountsForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void takeOrdersButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            takeOrdersForm page = new takeOrdersForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void performAttendenceButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            performAttendenceForm page = new performAttendenceForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void generateBillButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            generateBillForm page = new generateBillForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }
    }
}
