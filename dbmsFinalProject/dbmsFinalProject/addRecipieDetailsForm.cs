﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Runtime.InteropServices;



namespace dbmsFinalProject
{
    public partial class addRecipieDetailsForm : Form
    {


                    [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

                    private static extern IntPtr CreateRoundRectRgn
            (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse

            );

        public addRecipieDetailsForm()
        {
            InitializeComponent();
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 25, 25));

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            employeeForm page = new employeeForm();
            page.Closed += (s, args) => this.Close();
            page.Show();
        }

        private void addRecipieDetailsForm_Load(object sender, EventArgs e)
        {
            //Product Name get
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select [ProductName] as PName,[ProductID] as ID from Products ", con);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            productIDComboBox.DisplayMember = "PName";
            productIDComboBox.ValueMember = "ID";
            productIDComboBox.DataSource = dt;


            //Raw MaterialID Get
            SqlCommand cmd2 = new SqlCommand("Select Raw_MaterialID from [Raw_Materials] ", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter daa = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            daa.Fill(dt2);
            foreach (DataRow dr in dt2.Rows)
            {

                rawMaterialIDComboBox.Items.Add(dr["Raw_MaterialID"].ToString());


            }




        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from [Recipie Details] ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            recipieDetailsDataGridView.DataSource = dt;
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Delete from [Recipie Details] where IngredientID=@IngredientID  and ProductID=@ProductID", con);
            cmd1.Parameters.AddWithValue("@ProductID", productIDComboBox.SelectedValue);
            cmd1.Parameters.AddWithValue("@IngredientID", int.Parse(rawMaterialIDComboBox.Text));
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Successfully Deleted Data");
            productIDComboBox.Text = "";
            rawMaterialIDComboBox.Text = "";
        }

        private void addButton_Click(object sender, EventArgs e)
        {

            if (System.Text.RegularExpressions.Regex.IsMatch(rawMaterialIDComboBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in phone");
                rawMaterialIDComboBox.Text = rawMaterialIDComboBox.Text.Remove(rawMaterialIDComboBox.Text.Length - 1);

            }
            else if (productIDComboBox.Text == "" || rawMaterialIDComboBox.Text == "" || descriptionTextBox.Text == "")
            {

                MessageBox.Show("All Fields should be filled");
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into [Recipie Details] values(@ProductID,@IngredientID,@Description)", con);
                cmd.Parameters.AddWithValue("@ProductID", productIDComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@IngredientID", int.Parse(rawMaterialIDComboBox.Text));
                cmd.Parameters.AddWithValue("@Description", descriptionTextBox.Text);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {


            if (System.Text.RegularExpressions.Regex.IsMatch(rawMaterialIDComboBox.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers in phone");
                rawMaterialIDComboBox.Text = rawMaterialIDComboBox.Text.Remove(rawMaterialIDComboBox.Text.Length - 1);

            }
            else if (productIDComboBox.Text == "" || rawMaterialIDComboBox.Text == "" || descriptionTextBox.Text == "")
            {

                MessageBox.Show("All Fields should be filled");
            }
            else
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update  [Recipie Details] set Description=@Description where ProductID=@ProductID and IngredientID=@IngredientID", con);

                cmd.Parameters.AddWithValue("@ProductID", productIDComboBox.SelectedValue);
                cmd.Parameters.AddWithValue("@IngredientID", int.Parse(rawMaterialIDComboBox.Text));
                cmd.Parameters.AddWithValue("@Description", descriptionTextBox.Text);

                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Updated");
            }
        }

       
    }
}
