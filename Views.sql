/*MONTLY ATTENDANCE ID's*/
CREATE VIEW ATTENDENT AS SELECT *  FROM [Attendence Details]  WHERE AttendenceID IN (SELECT AttendenceID FROM Attendence  AS T GROUP BY month(t.DateCreated) , AttendenceID)

/*SANITIZATION ID'S MONTHLY*/
CREATE VIEW SANITIZATIONS AS SELECT T1.SanitizationID , T1.DateAchieved , T1.Achieved , T1.EmployeeID  FROM [Sanitization Details] AS T1 WHERE T1.SanitizationID IN (SELECT SanitizationID FROM [Sanitization] as t2 WHERE month(t2.SanitizationDate) = MONTH(GETDATE()))

